package chessTests;

import static org.junit.Assert.*;

import org.junit.Test;

import chessBoard.Board;
import chessPieces.Bishop;
import chessPieces.Knight;
import chessPieces.Pawn;
import chessPieces.Queen;

public class ChessPieceTest {

	@Test
	public void pieceContructorTest() throws Exception{
		int player = 1;
		int row_position = 2;
		int col_position = 6;
		Queen queen = new Queen(player, row_position, col_position);
		assertEquals(player, queen.getPlayer());
		assertEquals(row_position, queen.getRowPosition());
		assertEquals(col_position, queen.getColPosition());
		assertEquals('q', queen.getType());
	}
	
	/**
	 * User intends to move a piece to valid empty space.
	 * @throws Exception
	 */
	@Test
	public void pieceSetTest() throws Exception{
		Knight knight = new Knight(0, 5, 3);
		int new_row_position = 0;
		int new_col_position = 7;
		knight.setRowPosition(new_row_position);
		knight.setColPosition(new_col_position);
		assertEquals(new_row_position, knight.getRowPosition());
		assertEquals(new_col_position, knight.getColPosition());
	}
	
	
	@Test
	public void pieceRuleTest() throws Exception{
		Pawn pawn = new Pawn(0,6,0);
		int new_row_position = 4;
		int new_col_position = 3;
		boolean valid = pawn.checkRule(new_row_position, new_col_position);
		assertEquals(false, valid);
	}
	
	
	/**
	 * User intends to move a piece off board.
	 * @throws Exception
	 */
	@Test
	public void pieceInBoardTest() throws Exception{
		int row_position = 8;
		int col_position = 8;
		Bishop bishop = new Bishop(0, row_position, col_position);
		assertEquals(false, Board.isInBoard(bishop.getRowPosition(), bishop.getColPosition()));
	}

}
