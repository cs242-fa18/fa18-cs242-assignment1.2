package chessTests;

import java.util.*;
import static org.junit.Assert.*;
import org.junit.Test;

import chessPieces.ChessPiece;
import chessPieces.CustomNightrider;
import chessBoard.Board;
import chessPieces.CustomHopper;

public class CustomerPieceTest {

	
	/**
	 * Given a position, check simpleNightrider's rule.
	 * @throws Exception
	 */
	@Test
	public void simpleNightRuleTest() throws Exception{
		ChessPiece[][] testBoard = new ChessPiece[Board.SIZE][Board.SIZE];
		
		testBoard[4][4] = new CustomNightrider(0, 4, 4);
		
		assertEquals(true, testBoard[4][4].checkRule(2, 3));
		assertEquals(true, testBoard[4][4].checkRule(3, 2));
		assertEquals(true, testBoard[4][4].checkRule(2, 5));
		assertEquals(true, testBoard[4][4].checkRule(3, 6));
		assertEquals(true, testBoard[4][4].checkRule(5, 6));
		assertEquals(true, testBoard[4][4].checkRule(6, 5));
		assertEquals(true, testBoard[4][4].checkRule(6, 3));
		assertEquals(true, testBoard[4][4].checkRule(5, 2));
		
		assertEquals(false, testBoard[4][4].checkRule(1, 0));
		assertEquals(false, testBoard[4][4].checkRule(7, 5));
	}
	
	
	/**
	 * Check if all valid simpleNightrier's next positions collected are correct.
	 * @throws Exception
	 */
	@Test
	public void simpleNightPosTest() throws Exception{
		ChessPiece[][] testBoard = new ChessPiece[Board.SIZE][Board.SIZE];
		
		testBoard[4][4] = new CustomNightrider(0, 4, 4);
		
		ArrayList<int[]> possiblePos = testBoard[4][4].getNextPossiblePositions();
		for (int i=0; i<possiblePos.size(); i++) {
			int[] tempPos = possiblePos.get(i);
			System.out.println("The "+(i+1)+"th possible position is: "+tempPos[0]+" ,"+tempPos[1]);
		}
		
		assertEquals(12, possiblePos.size());
	}
	
	
	@Test
	public void simpleHopperRuleTest() throws Exception{
		ChessPiece[][] testBoard = new ChessPiece[Board.SIZE][Board.SIZE];
		testBoard[6][4] = new CustomHopper(1, 6, 4);
		assertEquals(true, testBoard[6][4].checkRule(4, 2));
		assertEquals(true, testBoard[6][4].checkRule(4, 4));
		assertEquals(true, testBoard[6][4].checkRule(4, 6));
		assertEquals(false, testBoard[6][4].checkRule(0, 0));
		assertEquals(false, testBoard[6][4].checkRule(7, 2));
	}
	
	
	@Test
	public void simpleHopperPosTest() throws Exception{
		ChessPiece[][] testBoard = new ChessPiece[Board.SIZE][Board.SIZE];
		testBoard[6][4] = new CustomHopper(1, 6, 4);
		ArrayList<int[]> nextPos = testBoard[6][4].getNextPossiblePositions();
		assertEquals(3, nextPos.size());
	}
	
}
