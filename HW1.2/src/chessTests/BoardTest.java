package chessTests;

import static org.junit.Assert.*;

import chessBoard.Board;
import chessPieces.ChessPiece;

import java.util.*;

import org.junit.Test;

public class BoardTest {

	@Test
	public void turnTest() throws Exception {
		Board board = new Board();
		assertEquals(board.getCurrentTurn(), 0);
	}
	
	
	@Test
	public void captureTest() throws Exception{
		Board board = new Board();
		ChessPiece pawn = board.getPieceAt(1,0);
		assertEquals(pawn.getType(), 'p');
		board.killPiece(pawn.getRowPosition(), pawn.getColPosition());
		assertEquals(pawn.isCaptured(), true);
		assertEquals(null, board.getPieceAt(1, 0));
	}
	
	
	@Test
	public void checkKingTest() throws Exception{
		Board board = new Board();
		int player = 1;
		assertEquals(false, board.isInCheck(player));
	}
	
	
	@Test
	public void validMovementTest() throws Exception{
		Board board = new Board();
		int player = 1;
		ChessPiece king = board.getKing(player);
		ArrayList<int[]> validMoves = board.validMoveFilter(player, king);
		int[] illegalPlace = new int[2];
		illegalPlace[0] = 6;
		illegalPlace[1] = 3;
		assertEquals(false, validMoves.contains(illegalPlace));
	}

	
	/**
	 * Check if a board is correctly initiated.
	 * @throws Exception
	 */
	@Test
	public void initiateBoard() throws Exception{
		Board board = new Board();
		
		//player 0's chess pieces
		//0 to 7 -> left to right
		ChessPiece black_pawn0 = board.getPieceAt(1, 0);
		ChessPiece black_pawn1 = board.getPieceAt(1, 1);
		ChessPiece black_pawn2 = board.getPieceAt(1, 2);
		ChessPiece black_pawn3 = board.getPieceAt(1, 3);
		ChessPiece black_pawn4 = board.getPieceAt(1, 4);
		ChessPiece black_pawn5 = board.getPieceAt(1, 5);
		ChessPiece black_pawn6 = board.getPieceAt(1, 6);
		ChessPiece black_pawn7 = board.getPieceAt(1, 7);
		
		ChessPiece black_rook0 = board.getPieceAt(0, 0);
		ChessPiece black_rook1 = board.getPieceAt(0, 7);
		
		ChessPiece black_knight0 = board.getPieceAt(0, 1);
		ChessPiece black_knight1 = board.getPieceAt(0, 6);
		
		ChessPiece black_bishop0 = board.getPieceAt(0, 2);
		ChessPiece black_bishop1 = board.getPieceAt(0, 5);
		
		ChessPiece black_queen = board.getPieceAt(0, 3);
		ChessPiece black_king = board.getPieceAt(0, 4);
		
		//player 1's chess pieces
		ChessPiece white_pawn0 = board.getPieceAt(6, 0);
		ChessPiece white_pawn1 = board.getPieceAt(6, 1);
		ChessPiece white_pawn2 = board.getPieceAt(6, 2);
		ChessPiece white_pawn3 = board.getPieceAt(6, 3);
		ChessPiece white_pawn4 = board.getPieceAt(6, 4);
		ChessPiece white_pawn5 = board.getPieceAt(6, 5);
		ChessPiece white_pawn6 = board.getPieceAt(6, 6);
		ChessPiece white_pawn7 = board.getPieceAt(6, 7);
		
		ChessPiece white_rook0 = board.getPieceAt(7, 0);
		ChessPiece white_rook1 = board.getPieceAt(7, 7);
		
		ChessPiece white_knight0 = board.getPieceAt(7, 1);
		ChessPiece white_knight1 = board.getPieceAt(7, 6);
		
		ChessPiece white_bishop0 = board.getPieceAt(7, 2);
		ChessPiece white_bishop1 = board.getPieceAt(7, 5);
		
		ChessPiece white_queen = board.getPieceAt(7, 3);
		ChessPiece white_king = board.getPieceAt(7, 4);
		
		//test
		assertEquals(black_pawn0.getType(), 'p');
		assertEquals(black_pawn1.getType(), 'p');
		assertEquals(black_pawn2.getType(), 'p');
		assertEquals(black_pawn3.getType(), 'p');
		assertEquals(black_pawn4.getType(), 'p');
		assertEquals(black_pawn5.getType(), 'p');
		assertEquals(black_pawn6.getType(), 'p');
		assertEquals(black_pawn7.getType(), 'p');
		
		assertEquals(black_rook0.getType(), 'r');
		assertEquals(black_rook1.getType(), 'r');
		
		assertEquals(black_bishop0.getType(), 'b');
		assertEquals(black_bishop1.getType(), 'b');
		
		assertEquals(black_knight0.getType(), 'k');
		assertEquals(black_knight1.getType(), 'k');
		
		assertEquals(black_queen.getType(), 'q');
		assertEquals(black_king.getType(), 'K');
		
		assertEquals(white_pawn0.getType(), 'p');
		assertEquals(white_pawn1.getType(), 'p');
		assertEquals(white_pawn2.getType(), 'p');
		assertEquals(white_pawn3.getType(), 'p');
		assertEquals(white_pawn4.getType(), 'p');
		assertEquals(white_pawn5.getType(), 'p');
		assertEquals(white_pawn6.getType(), 'p');
		assertEquals(white_pawn7.getType(), 'p');
		
		assertEquals(white_rook0.getType(), 'r');
		assertEquals(white_rook1.getType(), 'r');
		
		assertEquals(white_bishop0.getType(), 'b');
		assertEquals(white_bishop1.getType(), 'b');
		
		assertEquals(white_knight0.getType(), 'k');
		assertEquals(white_knight1.getType(), 'k');
		
		assertEquals(white_queen.getType(), 'q');
		assertEquals(white_king.getType(), 'K');
	}
	
	
	@Test
	public void moveToTest() throws Exception{
		Board board = new Board();
		
		//Player 0 goes first.
		ChessPiece black_pawn4 = board.getPieceAt(1, 3);
		
		//illegal move
		board.moveTo(black_pawn4, 8, 8);
		assertNotEquals(board.getPieceAt(1, 3), null);
		assertEquals(board.getPieceAt(2, 3), null);
		assertEquals(board.getPieceAt(1, 3).getType(), 'p');
		
		//Remains the same turn
		assertEquals(board.getCurrentTurn(), 0);
		
		assertEquals(black_pawn4.getPlayer(), 0);
		
		//Now try legal move for player 0: first move, move one step forward
		board.moveTo(black_pawn4, 2, 3);
		assertEquals(board.getPieceAt(1, 3), null);
		assertNotEquals(board.getPieceAt(2, 3), null);
		assertEquals(board.getPieceAt(2, 3).getType(), 'p');
		
		//Try legal first move for player 1: first move, move two steps forward
		ChessPiece white_pawn0 = board.getPieceAt(6, 0);
		assertEquals(board.getCurrentTurn(), 1);
		board.moveTo(white_pawn0, 4, 0);
		assertEquals(board.getPieceAt(6, 0), null);
		assertNotEquals(board.getPieceAt(4, 0), null);
		assertEquals(board.getPieceAt(4, 0).getType(), 'p');
		
		//Neither player is check-mating
		assertEquals(board.checkmateHelper(0), -1);
		assertEquals(board.checkmateHelper(1), -1);
		assertEquals(board.checkmate(), -1);
	}
	
}
