/**
* @author Dorothy Yu
* @version 1.00
*/

package chessPieces;

import java.util.*;

public abstract class ChessPiece {

  // Using 0,1 to represent different player
  protected int player;

  // Specific chessPiece type. e.g King, Queen ... 
  protected char type;

  protected int row_position;
  protected int col_position;

  // Represent chess piece's current validity 
  protected boolean captured;

  
  public ChessPiece(int player, int row_position, int col_position) {
    this.player = player;
    this.row_position = row_position;
    this.col_position = col_position;
    this.captured = false;
  }
  
  /**
   * Protected method for other classes to use
   * @return
   */
  public int getRowPosition(){
    return this.row_position;
  }

  
  public int getColPosition(){
    return this.col_position;
  }

  
  public boolean isCaptured(){
    return this.captured;
  }

  
  public int getPlayer(){
    return this.player;
  }

  
  public void setRowPosition(int row_position){
    this.row_position = row_position;
  }

  
  public void setColPosition(int col_position){
    this.col_position = col_position;
  }

  
  /**
   * When this chess piece is captured, delete it from the board.
   */
  public void killed(){
    this.captured = true;
    this.setRowPosition(-1);
    this.setColPosition(-1);
  }
  
  
  public char getType() {
	return this.type;
  }

  
  public abstract boolean checkRule(int row_position, int col_position);
  public abstract ArrayList<int[]> getNextPossiblePositions();

}
