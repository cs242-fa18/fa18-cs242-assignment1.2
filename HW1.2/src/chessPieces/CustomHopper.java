package chessPieces;

import java.util.*;
import static java.lang.Math.abs;
import chessBoard.Board;

public class CustomHopper extends ChessPiece{
	
	public CustomHopper(int player, int row_position, int col_position) {
		super(player, row_position, col_position);
		this.type = 'h';
	}
	
	
	/**
	 * Hopper only hops one-step forward in 3 directions (\|/).
	 * It doesn't care whether the cell that it hops across is empty.
	 * * Notice: checkRule assumes the given position is validated by isInBoard.
	 * * But if other positions are used within the method, checkRule will check their in-board condition.
	 * * Example: CustomNightrider's checkRule method.
	 */
	@Override
	public boolean checkRule(int row_pos, int col_pos) {	
		int forward_direct = getForwardDirect();
		
		if (this.col_position == col_pos && this.row_position+2*forward_direct == row_pos) {
			return true;
		} else if (this.row_position+2*forward_direct == row_pos && abs(this.col_position - col_pos) == 2) {
			return true;
		}
		return false;
	}


	private int getForwardDirect() {
		int forward_direct;
		
		if (this.player == 0) {
			forward_direct = 1;
		} else {
			forward_direct = -1;
		}
		return forward_direct;
	}
	
	
	/**
	 * CustomHopper could have at most 3 next possible positions.
	 */
	@Override
	public ArrayList<int[]> getNextPossiblePositions(){
		ArrayList<int[]> nextPos = new ArrayList<int[]>();
		int[] tempPos = new int[2];
		int forward_direct = this.getForwardDirect();
		
		if (Board.isInBoard(this.row_position+2*forward_direct, this.col_position)) {
			tempPos[0] = this.row_position+2*forward_direct;
			tempPos[1] = this.col_position;
			nextPos.add(tempPos);
		} 
		if (Board.isInBoard(this.row_position+2*forward_direct, this.col_position+2)) {
			tempPos[0] = this.row_position+2*forward_direct;
			tempPos[1] = this.col_position+2;
			nextPos.add(tempPos);
		}
		if (Board.isInBoard(this.row_position+2*forward_direct, this.col_position-2)) {
			tempPos[0] = this.row_position+2*forward_direct;
			tempPos[1] = this.col_position-2;
			nextPos.add(tempPos);
		}
		
		return nextPos;
	}
}