package chessPieces;

import java.util.*;

import chessBoard.Board;

import static java.lang.Math.abs;

public class Bishop extends ChessPiece{
	
	public Bishop(int player, int row_position, int col_position) {
		super(player, row_position, col_position);
		this.type = 'b';
	}
	
	
	/**
	 * Bishop only moves diagonally.
	 */
	@Override
	public boolean checkRule(int row_position, int col_position) {
		return abs(row_position - this.row_position) == abs(col_position - this.col_position);
	}
	
	@Override
	public ArrayList<int[]> getNextPossiblePositions(){
		ArrayList<int[]> nextPos = new ArrayList<int[]>();
		
		// Loop through board to find possible places i.e satisfy rule and is in board.
		for (int row = 0; row < Board.SIZE; row++) {
			for (int col = 0; col < Board.SIZE; col++) {
				int[] tempPos = new int[2];
				if (Board.isInBoard(row, col)) {
					if (checkRule(row, col)) {
						tempPos[0] = row;
						tempPos[1] = col;
						nextPos.add(tempPos);
					}
				}
			}
		}
		return nextPos;
	}
	
}