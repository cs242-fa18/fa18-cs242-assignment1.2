package chessPieces;

import java.util.*;

import chessBoard.Board;

public class Rook extends ChessPiece{
	public Rook (int player, int row_position, int col_position) {
		super(player, row_position, col_position);
		this.type = 'r';
	}
	
	
	/**
	 * Rook only moves along file or rank with any steps.
	 */
	@Override
	public boolean checkRule(int row_position, int col_position) {
		return (row_position == this.row_position)||(col_position == this.col_position);
	}
	
	
	@Override
	public ArrayList<int[]> getNextPossiblePositions(){
		ArrayList<int[]> nextPos = new ArrayList<int[]>();
		
		// File check to find possible moves
		for (int row = 0; row < Board.SIZE; row++) {
			int[] tempPos = new int[2];
			if (Board.isInBoard(row, this.col_position)) {
				if (row != this.row_position) {
					tempPos[0] = row;
					tempPos[1] = this.col_position;
					nextPos.add(tempPos);
				}
			}
		}
		
		// Rank check to find possible moves
		for (int col = 0; col < Board.SIZE; col++) {
			int[] tempPos = new int[2];
			if (Board.isInBoard(this.row_position, col)) {
				if (col != this.col_position) {
					tempPos[0] = this.row_position;
					tempPos[1] = col;
					nextPos.add(tempPos);
				}
			}
		}
		
		return nextPos;
	}
	
}