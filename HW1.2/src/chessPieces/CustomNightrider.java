
package chessPieces;

import java.util.*;

import chessBoard.Board;

public class CustomNightrider extends ChessPiece {
	
	//Position_queue is specifically for calls within CustomNightrider.
	private ArrayList<int[]> position_queue = new ArrayList<int[]>();
	
	public CustomNightrider(int player, int row_position, int col_position){
	    super(player, row_position, col_position);
	    this.type = 'n';
	  }
	
	
	/**
	 * Helper function to check whether position given is the same as the chess piece's current position.
	 * @param row_position
	 * @param col_position
	 * @return
	 */
	private boolean positionIsMatched(int row_position, int col_position) {
		return (this.row_position == row_position && this.col_position == col_position);
	}
	
	
	/**
	 * Helper function: first check if the next position is in board,
	 * then check if it matches chess piece's current position, return true.
	 * Otherwise, add this position and it's moving direction as a integer array tempPos to position_queue.
	 * @param next_row_position: tempPos[0]
	 * @param next_col_position: tempPos[1]
	 * @param row_direct: tempPos[2], row's moving direction
	 * @param col_direct: tempPos[3], col's moving direction
	 * @return
	 */
	private boolean checkRuleHelper(int next_row_position, int next_col_position, int row_direct, int col_direct) {
		if (Board.isInBoard(next_row_position+row_direct, next_col_position+col_direct)) {
			if (this.positionIsMatched(next_row_position+row_direct, next_col_position+col_direct)) {
				return true;
			} else {
				int[] tempPos = new int[4];
				tempPos[0] = next_row_position+row_direct;
				tempPos[1] = next_col_position+col_direct;
				tempPos[2] = row_direct;
				tempPos[3] = col_direct;
				position_queue.add(tempPos);
			}
		}
		return false;
	}
	
	
	/**
	 * The simple night rider moves any number of steps as a knight in the same direction.
	 * It does not require vacant in-between cells to complete the movement.
	 * CheckRule is supposed to be called after in-board checking.
	 * @return Whether the new position satisfies the rule.
	 */
	@Override
	public boolean checkRule(int row_position, int col_position) {
		
		//Use this boolean as a flag to avoid code redundancy.
		boolean satisfied = false; 
		
		/**
		 * Starting from new position, search for each direction.
		 * If found current position matching chess piece's position during searching, then empty the queue and return true
		 * Else, if this position is in board, push this position to the queue.
		 * And loop through queue to find other possible positions in the same direction with current one.
		 * If no matching appears, and queue is empty, then return false.
		 */
		if (this.checkRuleHelper(row_position, col_position, -1, -2)) {
			satisfied = true;
		} else if (this.checkRuleHelper(row_position, col_position, -1, 2)) {
			satisfied = true;
		} else if (this.checkRuleHelper(row_position, col_position, 1, -2)) {
			satisfied = true;
		} else if (this.checkRuleHelper(row_position, col_position, 1, 2)) {
			satisfied = true;
		} else if (this.checkRuleHelper(row_position, col_position, -2, 1)) {
			satisfied = true;
		} else if (this.checkRuleHelper(row_position, col_position, -2, -1)) {
			satisfied = true;
		} else if (this.checkRuleHelper(row_position, col_position, 2, -1)) {
			satisfied = true;
		} else if (this.checkRuleHelper(row_position, col_position, 2, 1)) {
			satisfied = true;
		}
		
		if (satisfied) {
			//clean position_queue and return true
			position_queue.clear();
			return true;
		} else {
			satisfied = false;
			while (!position_queue.isEmpty()) {
				int[] tempPos = position_queue.remove(0);
				if (this.checkRuleHelper(tempPos[0], tempPos[1], tempPos[2], tempPos[3])) {
					satisfied = true;
				}
			}
		}
		return satisfied;
		
	}
	
	
	/**
	 * This function doesn't know about board conditions other than isInBoard.
	 */
	@Override
	public ArrayList<int[]> getNextPossiblePositions(){
		ArrayList<int[]> nextPos = new ArrayList<int[]>();
		
		//Loop through the whole board to find positions that are valid through checkRule.
		for (int row=0; row<Board.SIZE; row++) {
			for (int col=0; col<Board.SIZE; col++) {
				int[] tempPos = new int[2];
				if (Board.isInBoard(row, col) && this.checkRule(row, col)) {
					tempPos[0] = row;
					tempPos[1] = col;
					nextPos.add(tempPos);
				}
			}
		}
		
		position_queue.clear();
		return nextPos;
	}
}