package chessPieces;

import java.util.*;

import chessBoard.Board;

import static java.lang.Math.abs;

public class Knight extends ChessPiece{
	public Knight(int player, int row_position, int col_position) {
		super(player, row_position, col_position);
		this.type = 'k';
	}
	
	
	/**
	 * Check if new position satisfies a vertical or horizontal 'L'. 
	 */
	@Override
	public boolean checkRule(int row_position, int col_position) {
		return ( (abs(row_position - this.row_position) == 1 && abs(col_position - this.col_position) == 2) || (abs(row_position - this.row_position)==2 && abs(col_position - this.col_position) == 1));
	}
	
	
	/**
	 * This function doesn't know about board conditions other than if a position is in board.
	 */
	@Override
	public ArrayList<int[]> getNextPossiblePositions(){
		ArrayList<int[]> nextPos = new ArrayList<int[]>();
		
		// Set boundaries to avoid large loop.
		int row_lower_bound = this.row_position - 2;
		int row_higher_bound  = this.row_position + 2;
		int col_lower_bound = this.col_position - 2;
		int col_higher_bound = this.col_position + 2;
		
		// Loop through to find next possible moves.
		for (int row = row_lower_bound; row <= row_higher_bound; row++) {
			for (int col = col_lower_bound; col <= col_higher_bound; col++) {
				int[] tempPos = new int[2];
				if (Board.isInBoard(row, col)) {
					if (checkRule(row, col)) {
						tempPos[0] = row;
						tempPos[1] = col;
						nextPos.add(tempPos);
					}
				}
			}
		}
		return nextPos;
	}
	
}