
package chessPieces;

import java.util.*;

import chessBoard.Board;

import static java.lang.Math.abs;

public class King extends ChessPiece {
  public King(int player, int row_position, int col_position){
    super(player, row_position, col_position);
    this.type = 'K';
  }
  
  
  /**
   * King moves 1 step to any direction.
   */
  @Override
  public boolean checkRule(int row_position, int col_position){
    return (abs(row_position - this.row_position) <= 1 && abs(col_position - this.col_position) <= 1);
  }

  
  @Override
  public ArrayList<int[]> getNextPossiblePositions(){

    ArrayList<int[]> nextPos = new ArrayList<int[]>();

    // Since king only moves one step, setting boundaries helps avoid large loop.
    int row_lower_bound = this.row_position - 1;
    int row_higher_bound = this.row_position + 1;
    int col_lower_bound = this.col_position - 1;
    int col_higher_bound = this.col_position + 1;

    //Loop through boundaries to find possible moves.
    for (int row = row_lower_bound; row <= row_higher_bound; row++){
      for (int col = col_lower_bound; col <= col_higher_bound; col++){
    	  	int[] tempPos = new int[2];
        if (Board.isInBoard(row, col)){
          if (checkRule(row, col)){
            tempPos[0] = row;
            tempPos[1] = col;
            nextPos.add(tempPos);
          }
        }
      }
    }
    return nextPos;
  }

}
