package chessPieces;

import java.util.*;

import chessBoard.Board;

import static java.lang.Math.abs;


public class Pawn extends ChessPiece{
	
	private boolean first_move = true;
	
	
	public Pawn(int player, int row_position, int col_position) {
		super(player, row_position, col_position);
		this.type = 'p';
	}

	
	@Override
	public boolean checkRule(int row_position, int col_position) {
		
		int forward_direction;
		
		// Top player moves downwards: row index increases;
		// Bottom player moves upwards: row index decreases. 
		if (this.player == 0) {
			forward_direction = 1;
		} else {
			forward_direction = -1;
		}
		
		// Check if input location satisfies rule of moving forward. 
		if (col_position == this.col_position) {
			if (this.first_move) {
				this.first_move = false;
				if (row_position == (this.row_position + forward_direction)
						||(row_position == (this.row_position + 2*forward_direction))) {
					return true;
				}
			} else {	 //not first move, can only forward 1 step
				if (row_position == (this.row_position + forward_direction)) {
					return true;
				}
			}
		} else if (row_position == (this.row_position + forward_direction) 
				&& abs(col_position - this.col_position) == 1) { // Check if input location satisfies rule of moving diagonally.
			return true;
		}
		return false;
	}
	
	
	@Override
	public ArrayList<int[]> getNextPossiblePositions(){
		ArrayList<int[]> nextPos = new ArrayList<int[]>();
		
		for (int row = 0; row < Board.SIZE; row++) {
			for (int col = 0; col < Board.SIZE; col++) {
				int[] tempPos = new int[2];
				if (Board.isInBoard(row, col)) {
					if (checkRule(row, col)) {
						tempPos[0] = row;
						tempPos[1] = col;
						nextPos.add(tempPos);
					}
				}
			}
		}
		return nextPos;
	}
}