/**
Below is a chess board graph for the ease of implementation.

k: knight
K: king

  0 1 2 3 4 5 6 7
0 r k b K q b k r
1 p p p p p p p p <------ player 0
2
3
4
5
6 p p p p p p p p <------ player 1
7 r k b K q b k r

Board class supports creating a game board for the game, and completing a series of 
chess pieces movements.
**/
package chessBoard;

//import java.io.Console;
import java.util.*;

import chessPieces.Bishop;
import chessPieces.ChessPiece;
import chessPieces.King;
import chessPieces.Knight;
import chessPieces.Pawn;
import chessPieces.Queen;
import chessPieces.Rook;

public class Board{

  public static int SIZE = 8;

  ChessPiece[][] board;

  // Current turn. Default: player 0 moves first. 
  protected int turn = 0;

 
  /**
   * Board constructor: initialize all chessPieces for both players.
   */
  public Board(){
    this.board = new ChessPiece[SIZE][SIZE];

    // Initiate chess for player 0 - black
    int rowIdx = 0;
    int player = 0;
    initiateBoardChessPieces(rowIdx, player);

    // Initiate chess for player 1 - white
    rowIdx = 7;
    player = 1;
    initiateBoardChessPieces(rowIdx, player);
    
    //Initiate pawns for both players.
    for (int col = 0; col < SIZE; col++){
        this.board[1][col] = new Pawn(0,1,col);
        this.board[6][col] = new Pawn(1,6,col);
      }
  }


  /**
   * Helper function.
   * @param rowIdx
   */
  private void initiateBoardChessPieces(int rowIdx, int player) {
	this.board[rowIdx][0] = new Rook(player,rowIdx,0);
	this.board[rowIdx][1] = new Knight(player,rowIdx,1);
    this.board[rowIdx][2] = new Bishop(player,rowIdx,2);
    this.board[rowIdx][3] = new Queen(player,rowIdx,3);
    this.board[rowIdx][4] = new King(player,rowIdx,4);
    this.board[rowIdx][5] = new Bishop(player,rowIdx,5);
    this.board[rowIdx][6] = new Knight(player,rowIdx,6);
    this.board[rowIdx][7] = new Rook(player,rowIdx,7);
  }

  
  /**
   * This functions checks if an input location satisfies board boundaries.
   * @param row_position
   * @param col_position
   * @return
   */
  public static boolean isInBoard(int row_position, int col_position){
    return row_position >= 0 && row_position < SIZE && col_position >= 0 && col_position < SIZE;
  }

  
  /**
   * This function updates board and related chessPieces if a player moves one chess piece.
   * Includes a helper function
   * @param chessPiece
   * @param row_position
   * @param col_position
   */
  public void moveTo(ChessPiece chessPiece, int row_position, int col_position) {
	  
	  //If new position is outside the board, then do nothing
	  //Remain the same turn
	  if (!isInBoard(row_position, col_position)) {
		  return;
	  }
	  
	  //If new position is possessed by the same player, then do nothing
	  //Remain the same turn
	  if (this.getPieceAt(row_position, col_position) != null && this.getPieceAt(row_position, col_position).getPlayer() == this.turn) {
		  return;
	  }
	  
	  //If new position is legal for this chessPiece
	  if (chessPiece.checkRule(row_position, col_position)) {
		  
		  //Check if it's empty, then move and Update directly
		  //Else if it's possessed by the opponent, then delete the opponent, and update
		  if (this.board[row_position][col_position] != null) {
			 this.killPiece(row_position, col_position);
		  }
		  this.moveHelper(chessPiece, row_position, col_position);
		  
		  //Update turn
		  this.turn = 1-this.turn;
	  }
  }
  
  
  /**
   * This is the helper function to directly move one piece to a place in the board, including
   * updating the board and the living chessPiece.
   * It doesn't check any conditions, and it should NOT be used outside this class.
   * @param chessPiece
   * @param row_position
   * @param col_position
   */
  protected void moveHelper(ChessPiece chessPiece, int row_position, int col_position) {
	  
	  //Move and update board
	  int old_row_position = chessPiece.getRowPosition();
	  int old_col_position = chessPiece.getColPosition();
	  this.board[old_row_position][old_col_position] = null;
	  this.board[row_position][col_position] = chessPiece;
	 
	  //Update living chessPiece
	  chessPiece.setRowPosition(row_position);
	  chessPiece.setColPosition(col_position);
  }
  
  
  /**
   * This function removes a piece from board, and update board
   * @param row_position
   * @param col_position
   */
  public void killPiece(int row_position, int col_position) {
	  this.board[row_position][col_position].killed();
	  this.board[row_position][col_position] = null;
  }
  
  
  /**
   * Check if king of an input player is putting in check.
   * * TA's suggestion: use moveTo
   * @param player
   * @return
   */
  public boolean isInCheck(int player){
	  
	 int[] kingPos = getKingPosition(player);
	 ArrayList<ChessPiece> opponentPieces = getOpponentPieces(player);
	 
	 // Loop through all opponent possible moves, check if at least one of the 
	 // opponent is checking input player's king.
	 for (int i = 0; i < opponentPieces.size(); i++) {
		 ChessPiece currentOpponent = opponentPieces.get(i);
		 ArrayList<int[]> possibleMoves = currentOpponent.getNextPossiblePositions();
		 for (int j = 0; j < possibleMoves.size(); j++) {
			 int[] currentPos = possibleMoves.get(j);
			 if (currentPos[0] == kingPos[0] && currentPos[1] == kingPos[1]) {
				 System.out.println("Player " + player + " is in check!");
				 return true;
			 }
		 }
	 }
	  
	  return false;
  }
  
  
  /**
   * If the games ends, then returns the winner, else returns -1.
   * This function uses a helper function: checkmateHelper.
   * @return
   */
  public int checkmate() {

	  if (isInCheck(0)) {	// Player 0
		  return checkmateHelper(0);
		  
	  } else if (isInCheck(1)) {	// Player 1
		  return checkmateHelper(1);
	  }
	  
	  return -1;
  }
  
  
  /**
   * Helper function: This function decides whether there's no
   * legalMoves possible for king w.r.t input player. If none is left,
   * end game, print and return winner.
   * * TA's suggestion: in a game, remember that moving other chess pieces may save the king.
   * @param player
   * @return
   */
  public int checkmateHelper(int player) {
	  
	  ChessPiece king;
	  int legalPlaces;
	  int illegalCount = 0;
	  
	  king = getKing(player);
	  
	  ArrayList<int[]> legalMoves = king.getNextPossiblePositions();
	  legalPlaces = legalMoves.size(); 
			  
	  ArrayList<ChessPiece> opponentPieces = getOpponentPieces(player);
	  
	  // Check for each legal move of king whether it is already targeted by any opponent chessPiece.
	  for (int i = 0; i < legalPlaces; i++) {
		  for (int j = 0; j < opponentPieces.size(); j++) {
			  ArrayList<int[]> opponentMoves = opponentPieces.get(i).getNextPossiblePositions();
			  if (opponentMoves.contains(legalMoves.get(i))) {
				  illegalCount ++;
			  }
		  }
	  }
	  
	  // All legal places are targeted, king cannot escape. End of game.
	  if (legalPlaces <= illegalCount) {
		  System.out.println("Checkmate player "+ player+ ", Player "+ (1-player) +" won!");
		  return (1-player);
	  }
	  return -1;
  }
  
  
  /**
   * Helper function: return a chessPiece at an input location
   * @param row_position
   * @param col_position
   */
  public ChessPiece getPieceAt(int row_position, int col_position) {
	  return this.board[row_position][col_position];
  }
  
  
  /**
   * Helper function: return King location of an input player.
   * @param player
   * @return
   */
  public int[] getKingPosition(int player) {
	  int[] kingPos = new int[2];
	  kingPos[0] = -1;
	  
	  
	  ChessPiece king = getKing(player);
	  
	  if (king != null) {
		  kingPos[0] = king.getRowPosition();
		  kingPos[1] = king.getColPosition();
	  }
	  
	  // Defensive coding: just in case king has been mistakenly removed but
	  // game hasn't ended yet.
	  if (kingPos[0] != -1) {
		  return kingPos;
	  } else {
		  return null;
	  }
  }
  
  
  /**
   * Helper function: return the whole king piece w.r.t a player.
   * @param player
   * @return
   */
  public ChessPiece getKing(int player) {
	  ChessPiece king;
	  
	  // Loop through board to find king for player.
	  for (int row = 0; row < SIZE; row++) {
		  for (int col = 0; col < SIZE; col++) {
			  ChessPiece tempPiece = getPieceAt(row, col);
			  if (tempPiece != null && tempPiece.getType() == 'K' && tempPiece.getPlayer() == player) {
				  king = tempPiece;
				  return king;
			  }
		  }
	  }
	  return null;
  }
  
  
  /**
   * Helper function: With respect to current player, get all its 
   * alive opponent pieces in an array.
   * @param player
   * @return
   */
  public ArrayList<ChessPiece> getOpponentPieces(int player){
	  
	  ArrayList<ChessPiece> opponentPieces = new ArrayList<ChessPiece>();
	  
	  int opponent = 1-player;
	  
	  // loop through board to add all valid chess pieces belonged to opponent to an array. 
	  for (int row = 0; row < SIZE; row++) {
		  for (int col = 0; col < SIZE; col++) {
			  ChessPiece temp = getPieceAt(row, col);
			  if (temp != null && temp.getPlayer() == opponent) {
				  opponentPieces.add(temp);
			  }
		  }
	  }
	  
	  if (!opponentPieces.isEmpty()) {
		  return opponentPieces;
	  } else {
		  return null;
	  }
  }
  
  
  /**
   * Helper function: make sure one won't capture its own piece. Restricting valid movements to empty spaces and opponent capturing.
   * @param player
   * @param chessPiece
   * @return
   */
  public ArrayList<int[]> validMoveFilter(int player, ChessPiece chessPiece){
	  
	  ArrayList<int[]> validMoves = new ArrayList<int[]>();
	  
	  // Get moves which are possible for chessPiece i.e satisfies rules
	  ArrayList<int[]> possibleMoves = chessPiece.getNextPossiblePositions();
	  
	  // Get rid of places occupied by player himself/herself.
	  for (int i = 0; i < possibleMoves.size(); i++) {
		  int[] tempPos = possibleMoves.get(i);
		  ChessPiece piece = getPieceAt(tempPos[0], tempPos[1]);
		  if (piece != null && piece.getPlayer() != player) {
			  validMoves.add(tempPos);
		  }
	  }
	  return validMoves;
  }
  
 
  /**
   * For game to use.
   * @return
   */
  public int getCurrentTurn() {
	  return this.turn;
  }
  
  
  /**
   * Update turn of a game. For game to use.
   * @param turn
   */
  public void setTurn(int turn) {
	  this.turn = turn;
  }
  
}
