package graphGUI;

import javax.imageio.ImageIO;
import javax.swing.*;

import chessBoard.Board;

import java.awt.*;
import java.awt.image.BufferedImage;
//import java.io.File;
import java.io.IOException;
import java.util.*;

public class StaticGUI {
	
	private JFrame gameFrame;
	private BoardPanel boardPanel;
	
	private static Dimension WINDOW_DIMENSION = new Dimension(600, 600);
	private static Dimension BOARD_DIMENSION = new Dimension(400, 400);
	private static Dimension TILE_DIMENSION = new Dimension(10, 10);
	
	private Color light_tile_color = Color.decode("#FDEBD0");
	private Color dark_tile_color = Color.decode("#CC9966");
		
	
	/**
	 * This is the canvas for a chess board.
	 */
	public StaticGUI(){
		
		this.gameFrame = new JFrame("Chess Board");
		this.gameFrame.setSize(WINDOW_DIMENSION);
		this.gameFrame.setLayout(new BorderLayout());
		
		this.boardPanel = new BoardPanel();
		this.gameFrame.add(this.boardPanel, BorderLayout.CENTER);
		this.gameFrame.setVisible(true);
		
	}
	
	
	/**
	 * Board Panel contains default 64 Tile Panels.
	 * @author Dorothy
	 *
	 */
	private class BoardPanel extends JPanel{
		
		//Default serial versionUID
		private static final long serialVersionUID = 1L;
		private ArrayList<TilePanel> boardTiles;
		
		BoardPanel(){
			super(new GridLayout(Board.SIZE, Board.SIZE));
			this.boardTiles = new ArrayList<TilePanel>();

			for (int rowId = 0; rowId < Board.SIZE; rowId ++) {
				for (int colId = 0; colId < Board.SIZE; colId ++) {
					TilePanel tilePanel = new TilePanel(this, rowId, colId);
					this.boardTiles.add(tilePanel);
					this.add(tilePanel);
				}
			}
			
			this.setPreferredSize(BOARD_DIMENSION);
			this.validate();
		}
	}
	
	
	/**
	 * Used by board panel, will change when game starts.
	 * @author Dorothy
	 *
	 */
	private class TilePanel extends JPanel{
		
		//Default serial versionUID
		private static final long serialVersionUID = 1L;
		private int rowId;
		private int colId;
		
		
		TilePanel(BoardPanel boardPanel, int rowId, int colId){
			super(new GridBagLayout());
			
			this.rowId = rowId;
			this.colId = colId;
			
			this.setPreferredSize(TILE_DIMENSION);
			this.assignTileColor();
			this.assignTileIcon();
			this.validate();
		}

		
		/**
		 * According to a standard chess board, color of each tile alternates conseutively.
		 */
		private void assignTileColor() {
			if (this.rowId % 2 == 0) {
				if (this.colId % 2 == 0) {
					this.setBackground(light_tile_color);
				} else {
					this.setBackground(dark_tile_color);
				}
			} else {
				if (this.colId % 2 == 0) {
					this.setBackground(dark_tile_color);
				} else {
					this.setBackground(light_tile_color);
				}
			}	
		}
		
		
		/**
		 * Initialize chess piece on the board display.
		 */
		private void assignTileIcon() {
			this.removeAll();
			
			String player = "undefined";
			BufferedImage img = null; //Use an image buffer to store source chess image
			
			try {
				if (this.rowId == 1) {
					img = ImageIO.read(getClass().getResource("/images/BP.gif"));
				} else if (this.rowId == 6) {
					 img = ImageIO.read(getClass().getResource("/images/WP.gif")); 
				} else {
					if (this.rowId == 0) {
						player = "B";
					} else if (this.rowId == 7){
						player = "W";
					} else {
						player = "undefined";
					}
					
					if (player != "undefined") {
						if (this.colId == 0 || this.colId == 7) {
							img = ImageIO.read(getClass().getResource("/images/"+player+"R.gif"));
						} else if (this.colId == 1 || this.colId == 6) {
							img = ImageIO.read(getClass().getResource("/images/"+player+"N.gif"));
						} else if (this.colId == 2 || this.colId == 5) {
							img = ImageIO.read(getClass().getResource("/images/"+player+"B.gif"));
						} else if (this.colId == 3) {
							img = ImageIO.read(getClass().getResource("/images/"+player+"Q.gif"));
						} else {
							img = ImageIO.read(getClass().getResource("/images/"+player+"K.gif"));
						}
					}
				}
				
				this.add(new JLabel(new ImageIcon(img)));
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}